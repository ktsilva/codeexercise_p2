﻿using NUnit.Framework;

namespace KlingerSilva.CodeExercise2.Tests
{
    [TestFixture]
    public class ExtensionMethodsTests
    {
        [Test]
        public void Description_InvokingMethodOnEnum_ReturnsDescriptionAttribute()
        {
            // Arrange
            var restriction = DietaryRestriction.Vegetarian;
            // Act
            string description = restriction.Description();
            // Assert
            Assert.AreEqual("vegetarian", description);
        }
    }
}
