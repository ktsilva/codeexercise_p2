﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace KlingerSilva.CodeExercise2.Tests
{
    [TestFixture]
    public class RestaurantTests
    {
        [Test]
        public void Constructor_ConstructionWithNullOrWhitespaceName_ThrowsArgumentNullException()
        {
            
            // Assert
            Assert.That(() => new Restaurant(null, 0),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("name"));

            Assert.That(() => new Restaurant(" ", 0),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("name"));

            Assert.That(() => new Restaurant(string.Empty, 0),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("name"));
        }

        [Test]
        public void Constructor_ConstructionWithValidParameters_SetPropertiesCorrectly()
        {
            // Arrange
            Restaurant sut;
            var expectedName = "Rest A";
            decimal expectedRating = 5.0M/5.0M;

            // Act
            sut = new Restaurant(expectedName, expectedRating);

            // Assert
            Assert.AreEqual(expectedName, sut.Name);
            Assert.AreEqual(expectedRating, sut.Rating);
        }

        [Test]
        public void AddMealToMenu_AttemptingToAddNull_ThrowsArgumentNullException()
        {
            // Arrange
            var sut = new Restaurant("Rest A", 5.0M / 5.0M);

            // Assert
            Assert.That(() => sut.AddMealToMenu(null),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("mealType"));
        }

        [Test]
        public void AddMealToMenu_AttemptingToAddSameMealInstanceMoreThanOnce_ThrowsArgumentException()
        {
            // Arrange
            var sut = new Restaurant("Rest A", 5.0M / 5.0M);
            var meal = new MealType(DietaryRestriction.FishFree, 10);

            // Act
            sut.AddMealToMenu(meal);

            // Assert
            Assert.That(() => sut.AddMealToMenu(meal),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
        }

        [Test]
        public void AddMealToMenu_AttemptingToAddDuplicatedRestriction_ThrowsArgumentException()
        {
            // Arrange
            var sut = new Restaurant("Rest A", 5.0M / 5.0M);
            var meal1 = new MealType(DietaryRestriction.FishFree, 1);
            var meal2 = new MealType(DietaryRestriction.FishFree, 2);

            // Act
            sut.AddMealToMenu(meal1);

            // Assert
            Assert.That(() => sut.AddMealToMenu(meal2),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
        }

        [Test]
        public void GetMealFromMenu_MealAdded_CanBeRetrieved()
        {
            // Arrange
            var sut = new Restaurant("Rest A", 5.0M/5.0M);
            var meal = new MealType(DietaryRestriction.FishFree, 10);

            // Act
            sut.AddMealToMenu(meal);
            var mealRetrieved = sut.GetMealFromMenu(meal.Restriction);

            // Assert
            Assert.AreSame(meal, mealRetrieved);
        }

        [TestCase(DietaryRestriction.Others, 1U, 1U, 1U, 0, Description = "Available same as requested")]
        [TestCase(DietaryRestriction.Others, 1U, 2U, 1U, 1, Description = "Available less than requested")]
        [TestCase(DietaryRestriction.Others, 2U, 1U, 1U, 0, Description = "Available more than requested")]
        [TestCase(DietaryRestriction.Vegetarian, 1U, 1U, 1U, 0, Description = "Available same as requested")]
        [TestCase(DietaryRestriction.Vegetarian, 1U, 2U, 1U, 1, Description = "Available less than requested")]
        [TestCase(DietaryRestriction.Vegetarian, 2U, 1U, 1U, 0, Description = "Available more than requested")]
        public void FulfillOrder_InvokingMethodOnRestaurantWithOneMealForOneRequest_ReturnsExpectedResult(DietaryRestriction restriction, uint availableQuantity, uint requestedQuantity, uint expectedQuantity, int backorderCount)
        {
            // Arrange
            var sut = new Restaurant("Restaurant A", 5.0M / 5.0M);
            sut.AddMealToMenu(new MealType(DietaryRestriction.Others, availableQuantity));
            sut.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, availableQuantity));

            IList<MealType> backorder;
            IList<MealType> mealsToProvide = new List<MealType>();
            var requestedMeal = new MealType(restriction, requestedQuantity);
            mealsToProvide.Add(requestedMeal);

            // Act
            var result = sut.FulfillOrder(mealsToProvide, out backorder);

            // Assert
            Assert.AreEqual(backorderCount, backorder.Count);
            Assert.AreEqual(1, result.MealsToOrder.Count);
            Assert.AreEqual(restriction, result.MealsToOrder[0].Restriction);
            Assert.AreEqual(expectedQuantity, result.MealsToOrder[0].Quantity);
        }

        [TestCase(DietaryRestriction.FishFree, 1U, Description = "Request meal not provided by restaurant results in backorder")]
        [TestCase(DietaryRestriction.FishFree, 10U, Description = "Request meal not provided by restaurant results in backorder")]
        public void FulfillOrder_InvokingMethodOnRestaurantWithOneMealForOneRequestThatIsNotProvidedByRestaurant_ProducesBackorderWithSameRequestedMeal(DietaryRestriction restriction, uint requestedQuantity)
        {
            // Arrange
            var sut = new Restaurant("Restaurant A", 5.0M / 5.0M);
            sut.AddMealToMenu(new MealType(DietaryRestriction.Others, 1U));
            sut.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 1U));

            IList<MealType> backorder;
            IList<MealType> mealsToProvide = new List<MealType>();
            var requestedMeal = new MealType(restriction, requestedQuantity);
            mealsToProvide.Add(requestedMeal);

            // Act
            var result = sut.FulfillOrder(mealsToProvide, out backorder);

            // Assert
            Assert.AreEqual(1, backorder.Count);
            Assert.AreEqual(requestedQuantity, backorder[0].Quantity);
            Assert.AreEqual(restriction, backorder[0].Restriction);
            Assert.AreEqual(0, result.MealsToOrder.Count);
        }

        [TestCase(DietaryRestriction.FishFree, 1U, Description = "Request meal for restaurant without meals results in backorder")]
        [TestCase(DietaryRestriction.FishFree, 10U, Description = "Request meal for restaurant without meals results in backorder")]
        public void FulfillOrder_InvokingMethodOnRestaurantWithNoMealsAvailableForOneRequest_ProducesBackorderWithSameRequestedMeal(DietaryRestriction restriction, uint requestedQuantity)
        {
            // Arrange
            var sut = new Restaurant("Restaurant A", 5.0M / 5.0M);

            IList<MealType> backorder;
            IList<MealType> mealsToProvide = new List<MealType>();
            var requestedMeal = new MealType(restriction, requestedQuantity);
            mealsToProvide.Add(requestedMeal);

            // Act
            var result = sut.FulfillOrder(mealsToProvide, out backorder);

            // Assert
            Assert.AreEqual(1, backorder.Count);
            Assert.AreEqual(requestedQuantity, backorder[0].Quantity);
            Assert.AreEqual(restriction, backorder[0].Restriction);
            Assert.AreEqual(0, result.MealsToOrder.Count);
        }
    }
}
