﻿using System;
using System.Linq;
using NUnit.Framework;

namespace KlingerSilva.CodeExercise2.Tests
{
    [TestFixture]
    public class RestaurantOrderTests
    {
        [Test]
        public void Constructor_InstantiatoinWithZeroQuantity_ThrowsArgumentNullException()
        {
            // Assert
            Assert.That(() => new RestaurantOrder(null),
                    Throws.Exception
                      .TypeOf<ArgumentNullException>()
                      .With.Property("ParamName").EqualTo("restaurant"));
        }

        [Test]
        public void Constructor_ConstructionWithValidParameters_SetPropertiesCorrectly()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);

            // Act
            sut = new RestaurantOrder(restaurant);

            // Assert
            Assert.AreSame(restaurant, sut.Restaurant);
            Assert.IsEmpty(sut.MealsToOrder);
        }

        [Test]
        public void AddMealToOrder_AttemptingToAddNull_ThrowsArgumentNullException()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);
            sut = new RestaurantOrder(restaurant);

            // Assert
            Assert.That(() => sut.AddMealToOrder(null),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("mealType"));
        }

        [Test]
        public void AddMealToOrder_MealAdded_CanBeRetrieved()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);
            sut = new RestaurantOrder(restaurant);
            var meal = new MealType(DietaryRestriction.FishFree, 10);
            
            // Act
            sut.AddMealToOrder(meal);

            // Assert
            Assert.IsTrue(sut.MealsToOrder.Contains(meal));
        }

        [Test]
        public void ToString_InvokingMethodForOrderWithNoMeals_ReturnsProperlyFormattedString()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);
            sut = new RestaurantOrder(restaurant);

            // Act
            string actual = sut.ToString();

            // Assert
            Assert.AreEqual("Rest A ()", actual);
        }

        [Test]
        public void ToString_InvokingMethodForOrderWithOneMeal_ReturnsProperlyFormattedString()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);
            sut = new RestaurantOrder(restaurant);
            var mealA = new MealType(DietaryRestriction.Others, 11);
            sut.AddMealToOrder(mealA);

            // Act
            string actual = sut.ToString();

            // Assert
            Assert.AreEqual("Rest A (11 others)", actual);
        }


        [Test]
        public void ToString_InvokingMethodForOrderWithMultipleMeals_ReturnsProperlyFormattedString()
        {
            // Arrange
            RestaurantOrder sut;
            var restaurant = new Restaurant("Rest A", 5.0M / 5.0M);
            sut = new RestaurantOrder(restaurant);
            sut.AddMealToOrder(new MealType(DietaryRestriction.Others, 11));
            sut.AddMealToOrder(new MealType(DietaryRestriction.Vegetarian, 20));

            // Act
            string actual = sut.ToString();

            // Assert
            Assert.AreEqual("Rest A (11 others + 20 vegetarian)", actual);
        }
    }
}
