﻿using System;
using NUnit.Framework;

namespace KlingerSilva.CodeExercise2.Tests
{
    [TestFixture]
    public class MealTypeTests
    {
        [Test]
        public void Constructor_IConstructionWithZeroQuantity_ThrowsArgumentException()
        {
            // Assert
            Assert.That(() => new MealType(DietaryRestriction.FishFree, 0),
                    Throws.Exception
                      .TypeOf<ArgumentException>()
                      .With.Message.StartsWith("Quantity must be greather than zero")
                      .With.Property("ParamName").EqualTo("quantity"));
        }

        [Test]
        public void Constructor_ConstructionWithValidParameters_SetPropertiesCorrectly()
        {
            // Arrange
            MealType sut;
            var expectedRestriction = DietaryRestriction.FishFree;
            uint expectedQuantity = 1;

            // Act
            sut = new MealType(expectedRestriction, expectedQuantity);

            // Assert
            Assert.AreEqual(expectedRestriction, sut.Restriction);
            Assert.AreEqual(expectedQuantity, sut.Quantity);
        }

        [TestCase(DietaryRestriction.Others, 1U, "1 others")]
        [TestCase(DietaryRestriction.Vegetarian, 10U, "10 vegetarian")]
        [TestCase(DietaryRestriction.FishFree, 100U, "100 fish free")]
        public void ToString_InvokingMethod_ReturnsProperlyFormattedString(DietaryRestriction restriction, uint quantity, string expectedString)
        {
            // Arrange
            MealType sut;

            // Act
            sut = new MealType(restriction, quantity);

            // Assert
            Assert.AreEqual(expectedString, sut.ToString());
        }
    }
}
