﻿using System;
using System.Linq;
using NUnit.Framework;

namespace KlingerSilva.CodeExercise2.Tests
{
    [TestFixture]
    public class TeamEventTests
    {
        [Test]
        public void AddSelectedRestaurant_AttemptingToAddNull_ThrowsArgumentNullException()
        {
            // Arrange
            var sut = new TeamEvent();

            // Assert
            Assert.That(() => sut.AddSelectedRestaurant(null),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("restaurant"));
        }

        [Test]
        public void AddSelectedRestaurant_AttemptingToAddDuplicates_ThrowsArgumentException()
        {
            // Arrange
            var tm = new TeamEvent();
            var restaurant = new Restaurant("Restaurant A", 5.0M / 5.0M);
            tm.AddSelectedRestaurant(restaurant);

            // Act / Assert
            Assert.That(() => tm.AddSelectedRestaurant(restaurant),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
            Assert.That(() => tm.AddSelectedRestaurant(new Restaurant("Restaurant A", 3.0M / 5.0M)),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
        }

        [Test]
        public void AddSelectedRestaurant_InvokeMethodWithValidValue_AddValue()
        {
            // Arrange
            var tm = new TeamEvent();
            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            var rB = new Restaurant("Restaurant B", 3.0M / 5.0M);

            // Act
            tm.AddSelectedRestaurant(rA);
            tm.AddSelectedRestaurant(rB);

            // Act / Assert
            Assert.AreSame(rA, tm.Restaurants[rA.Name]);
            Assert.AreSame(rB, tm.Restaurants[rB.Name]);
        }

        [Test]
        public void AddMealRequest_AttemptingToAddNull_ThrowsArgumentNullException()
        {
            // Arrange
            var sut = new TeamEvent();

            // Assert
            Assert.That(() => sut.AddMealRequest(null),
                   Throws.Exception
                         .TypeOf<ArgumentNullException>()
                         .With.Property("ParamName").EqualTo("mealType"));
        }

        [Test]
        public void AddMealRequest_AttemptingToAddDuplicates_ThrowsArgumentException()
        {
            // Arrange
            var tm = new TeamEvent();
            var meal = new MealType(DietaryRestriction.Others, 10U);
            tm.AddMealRequest(meal);

            // Act / Assert
            Assert.That(() => tm.AddMealRequest(meal),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
            Assert.That(() => tm.AddMealRequest(new MealType(DietaryRestriction.Others, 15U)),
                   Throws.Exception
                         .TypeOf<ArgumentException>());
        }

        [Test]
        public void AddMealRequest_InvokeMethodWithValidValue_AddValue()
        {
            // Arrange
            var tm = new TeamEvent();
            var mealO = new MealType(DietaryRestriction.Others, 10U);
            var mealV = new MealType(DietaryRestriction.Vegetarian, 1U);

            // Act
            tm.AddMealRequest(mealO);
            tm.AddMealRequest(mealV);

            // Act / Assert
            Assert.AreSame(mealO, tm.MealsRequested[mealO.Restriction]);
            Assert.AreSame(mealV, tm.MealsRequested[mealV.Restriction]);
        }

        [Test]
        public void CalculateOrders_InvokingMethodWithSAMPLEDATA_ProducesCorrectResults()
        {
            // Arrange
            var sut = new TeamEvent();
            
            // Create restaurantes
            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            rA.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 4));
            rA.AddMealToMenu(new MealType(DietaryRestriction.Others, 40 - 4));
            var rB = new Restaurant("Restaurant B", 3.0M / 5.0M);
            rB.AddMealToMenu(new MealType(DietaryRestriction.GlutenFree, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Others, 100 - 20 - 20));
            sut.AddSelectedRestaurant(rA);
            sut.AddSelectedRestaurant(rB);

            // Create team event
            sut.AddMealRequest(new MealType(DietaryRestriction.Vegetarian, 5));
            sut.AddMealRequest(new MealType(DietaryRestriction.GlutenFree, 7));
            sut.AddMealRequest(new MealType(DietaryRestriction.Others, 50 - 5 - 7));

            // Act
            var result = sut.CalculateOrders();
            string formattedResult = string.Join(", ", result.Select(m => m.ToString()).ToArray());
            
            // Assert
            Assert.AreEqual("Restaurant A (4 vegetarian + 36 others), Restaurant B (1 vegetarian + 7 gluten free + 2 others)", formattedResult);
        }

        [Test]
        public void CalculateOrders_InvokingMethodForRestaurantsWithoutMeals_ProducesNoOrders()
        {
            // Arrange
            var sut = new TeamEvent();

            // Create restaurantes
            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            var rB = new Restaurant("Restaurant B", 3.0M / 5.0M);
            sut.AddSelectedRestaurant(rA);
            sut.AddSelectedRestaurant(rB);

            // Create team event
            sut.AddMealRequest(new MealType(DietaryRestriction.Vegetarian, 5));
            sut.AddMealRequest(new MealType(DietaryRestriction.GlutenFree, 7));
            sut.AddMealRequest(new MealType(DietaryRestriction.Others, 50 - 5 - 7));

            // Act
            var result = sut.CalculateOrders();

            // Assert
            Assert.AreEqual(0, result.Count, "No orders should be fulfilled");
        }

        [Test]
        public void CalculateOrders_InvokingMethodForUnavailableMeals_ProducesNoOrders()
        {
            // Arrange
            var sut = new TeamEvent();

            // Create restaurantes
            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            rA.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 4));
            rA.AddMealToMenu(new MealType(DietaryRestriction.Others, 40 - 4));
            var rB = new Restaurant("Restaurant B", 3.0M / 5.0M);
            rB.AddMealToMenu(new MealType(DietaryRestriction.GlutenFree, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Others, 100 - 20 - 20));
            sut.AddSelectedRestaurant(rA);
            sut.AddSelectedRestaurant(rB);

            // Create team event
            sut.AddMealRequest(new MealType(DietaryRestriction.FishFree, 5));
            sut.AddMealRequest(new MealType(DietaryRestriction.NutFree, 7));

            // Act
            var result = sut.CalculateOrders();

            // Assert
            Assert.AreEqual(0, result.Count, "No orders should be fulfilled");
        }

        [Test]
        public void CalculateOrders_InvokingMethodWithoutRequestForEmptyRestaurant_ProducesNoOrders()
        {
            // Arrange
            var sut = new TeamEvent();

            // Act / Assert
            Assert.That(() => sut.CalculateOrders(),
                   Throws.Exception
                         .TypeOf<InvalidOperationException>()
                         .With.Message.EqualTo("Event has no selected restaurants"));
        }

        [Test]
        public void CalculateOrders_InvokingMethodWithoutMealsRequest_ProducesNoOrders()
        {
            // Arrange
            var sut = new TeamEvent();
            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            sut.AddSelectedRestaurant(rA);

            // Act / Assert
            Assert.That(() => sut.CalculateOrders(),
                   Throws.Exception
                         .TypeOf<InvalidOperationException>()
                         .With.Message.EqualTo("Event has no meals requested"));
        }
    }
}
