using System;
using System.Collections.Generic;

namespace KlingerSilva.CodeExercise2
{
    public class Restaurant
    {
        private readonly Dictionary<DietaryRestriction, MealType> _mealsAvailable = new Dictionary<DietaryRestriction, MealType>();

        public Restaurant(string name, decimal rating)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException("name");
            this.Name = name;
            this.Rating = rating;
        }

        public string Name { get; private set; }
        public decimal Rating { get; private set; }

        public void AddMealToMenu(MealType mealType)
        {
            if (mealType == null) throw new ArgumentNullException("mealType");
            _mealsAvailable.Add(mealType.Restriction, mealType);
        }

        public MealType GetMealFromMenu(DietaryRestriction restriction)
        {
            MealType typeToReturn;
            return _mealsAvailable.TryGetValue(restriction, out typeToReturn) ? typeToReturn : null;
        }

        public RestaurantOrder FulfillOrder(IEnumerable<MealType> mealsToProvide, out IList<MealType> backorder)
        {
            var order = new RestaurantOrder(this);

            backorder = new List<MealType>();
            foreach (var mealRequested in mealsToProvide)
            {
                var availableMealOnRestaurant = GetMealFromMenu(mealRequested.Restriction);

                if (availableMealOnRestaurant == null)
                {
                    backorder.Add(mealRequested);
                }
                else
                {
                    if (availableMealOnRestaurant.Quantity >= mealRequested.Quantity)
                    {
                        order.AddMealToOrder(new MealType(mealRequested.Restriction, mealRequested.Quantity));
                    }
                    else
                    {
                        order.AddMealToOrder(new MealType(mealRequested.Restriction, availableMealOnRestaurant.Quantity));
                        backorder.Add(new MealType(mealRequested.Restriction, mealRequested.Quantity - availableMealOnRestaurant.Quantity));
                    }
                }
            }
            return order;
        }
    }
}