﻿using System;
using System.ComponentModel;

namespace KlingerSilva.CodeExercise2
{
    [Flags]
    public enum DietaryRestriction
    {
        [Description("others")]
        Others = 0,
        [Description("vegetarian")]
        Vegetarian = 1,
        [Description("gluten free")]
        GlutenFree = 2,
        [Description("nut free")]
        NutFree = 4,
        [Description("fish free")]
        FishFree = 8
    }
}
