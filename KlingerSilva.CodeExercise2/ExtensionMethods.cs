﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace KlingerSilva.CodeExercise2
{
    public static class ExtensionMethods
    {
        public static string Description(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attr = field.GetCustomAttributes<DescriptionAttribute>().FirstOrDefault();
            return attr == null ? value.ToString() : attr.Description;
        }
    }
}
