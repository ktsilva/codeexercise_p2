﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KlingerSilva.CodeExercise2
{
    public class TeamEvent
    {
        private readonly Dictionary<string, Restaurant> _selectedRestaurants = new Dictionary<string, Restaurant>();
        private readonly Dictionary<DietaryRestriction, MealType> _mealsRequested = new Dictionary<DietaryRestriction, MealType>();

        public IReadOnlyDictionary<string, Restaurant> Restaurants { get { return _selectedRestaurants; } }
        public IReadOnlyDictionary<DietaryRestriction, MealType> MealsRequested { get { return _mealsRequested; } }

        public void AddSelectedRestaurant(Restaurant restaurant)
        {
            if (restaurant == null) throw new ArgumentNullException("restaurant");
            _selectedRestaurants.Add(restaurant.Name, restaurant);
        }

        public void AddMealRequest(MealType mealType)
        {
            if (mealType == null) throw new ArgumentNullException("mealType");
            _mealsRequested.Add(mealType.Restriction, mealType);
        }

        public IList<RestaurantOrder> CalculateOrders()
        {
            if (!_selectedRestaurants.Any()) throw new InvalidOperationException("Event has no selected restaurants");
            if (!_mealsRequested.Any()) throw new InvalidOperationException("Event has no meals requested");

            var orderResult = new List<RestaurantOrder>();
            var mealsToFulfill = _mealsRequested.Select(m => m.Value);
            var restaurantOrderedByRating = _selectedRestaurants.Select(r => r.Value).OrderByDescending(r => r.Rating);
            foreach (var restaurant in restaurantOrderedByRating)
            {
                IList<MealType> backorder;
                var mealOrder = restaurant.FulfillOrder(mealsToFulfill, out backorder);
                if (mealOrder.MealsToOrder.Any()) orderResult.Add(mealOrder);
                mealsToFulfill = backorder;
            }
            return orderResult;
        }
    }
}
