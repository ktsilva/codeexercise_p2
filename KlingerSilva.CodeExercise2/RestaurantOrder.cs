using System;
using System.Collections.Generic;
using System.Linq;

namespace KlingerSilva.CodeExercise2
{
    public class RestaurantOrder
    {
        private readonly List<MealType> _mealsToOrder = new List<MealType>();

        public RestaurantOrder(Restaurant restaurant)
        {
            if (restaurant == null) throw new ArgumentNullException("restaurant");
            this.Restaurant = restaurant;
        }

        public Restaurant Restaurant { get; private set; }

        public IReadOnlyList<MealType> MealsToOrder { get { return _mealsToOrder; } }

        public void AddMealToOrder(MealType mealType)
        {
            if (mealType == null) throw new ArgumentNullException("mealType");
            _mealsToOrder.Add(mealType);
        }

        public override string ToString()
        {
            string meals = string.Join(" + ", _mealsToOrder.Select(m => m.ToString()).ToArray());
            return string.Format("{0} ({1})", this.Restaurant.Name, meals);
        }
    }
}