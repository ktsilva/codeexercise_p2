using System;

namespace KlingerSilva.CodeExercise2
{
    public class MealType
    {
        public MealType(DietaryRestriction restriction, uint quantity)
        {
            if (quantity == 0) throw new ArgumentException("Quantity must be greather than zero", "quantity");
            this.Restriction = restriction;
            this.Quantity = quantity;
        }

        public DietaryRestriction Restriction { get; private set; }
        public uint Quantity { get; private set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Quantity, Restriction.Description());
        }
    }
}