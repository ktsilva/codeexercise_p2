﻿using System;
using System.Linq;

namespace KlingerSilva.CodeExercise2.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var teamLunch = new TeamEvent();

            var rA = new Restaurant("Restaurant A", 5.0M / 5.0M);
            rA.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 4));
            rA.AddMealToMenu(new MealType(DietaryRestriction.Others, 36));
            var rB = new Restaurant("Restaurant B", 3.0M / 5.0M);
            rB.AddMealToMenu(new MealType(DietaryRestriction.GlutenFree, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Vegetarian, 20));
            rB.AddMealToMenu(new MealType(DietaryRestriction.Others, 60));
            teamLunch.AddSelectedRestaurant(rA);
            teamLunch.AddSelectedRestaurant(rB);

            teamLunch.AddMealRequest(new MealType(DietaryRestriction.Vegetarian, 5));
            teamLunch.AddMealRequest(new MealType(DietaryRestriction.GlutenFree, 7));
            teamLunch.AddMealRequest(new MealType(DietaryRestriction.Others, 38));

            var result = teamLunch.CalculateOrders();
            string formattedResult = string.Join(", ", result.Select(m => m.ToString()).ToArray());

            Console.WriteLine(formattedResult);
        }
    }
}
