Author: Klinger Silva

Comments: 

Solution built using VS Community edition, on C#, .NET 4.5.2
The only dependency is on NUnit v3.0.1

Follow description of Problem:
We're ordering meals for a team lunch. Every member in the team needs one meal, some have dietary restrictions such as vegetarian, gluten free, nut free, and fish free. 
We have a list of restaurants which serve meals that satisfy some of these restrictions. Each restaurant has a rating, and a limited amount of meals in stock that they 
can make today. Implement an object oriented system that can automatically produce the best possible meal orders with reasonable assumptions.

Example:

Team needs: total 50 meals including 5 vegetarians and 7 glutein free.
Restaurants: Restautant A has a rating of 5/5 and can serve 40 meals including 4 vegetarians, 
Restautant B has a rating of 3/5 and can serve 100 meals including 20 vegetarians, and 20 glutein free.

Expected meal orders: Restaurant A (4 vegetarian + 36 others), Restaurant B (1 vegetarian + 7 glutein free + 2 others)